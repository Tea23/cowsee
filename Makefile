CC = gcc
SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)
CFLAGS = -Wall -O2 -lc -lm

all: clean cowsee

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

cowsee: ${OBJS}
	$(CC) ${OBJS} -o cowsee

clean:
	rm *.o cowsee >/dev/null 2>/dev/null || true