# cowsee! It's cowsay but in C!

I am learning C because of reasons, so this is my ridiculous attempt at cloning the all-powerful cowsay. But in C.

The original cowsay by Tony Monroe is found [here](https://github.com/tnalpgge/rank-amateur-cowsay).

This is very basic right now, and serves as a basic framework for what I want to do. At the time of writing, we are building consistent single line speech bubbles so long as the input is 40 characters or less. lmao.

By the time the project is complete, it will emulate the behaviour of the original cowsay entirely, being able to do the following:
* build cows using cow files (we'll aim for compatibility too!)
* word-wrap
* piping of programs (`echo lol | cowsay` should work)
* being the single greatest *nix command line tool of all time

This project is GPL3, in keeping with the license of the original.