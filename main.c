#include <stdio.h>
#include <string.h>
#include <math.h>

void cow(char *moo);
void bubble(char *moo);

// total number of characters per line, excepting bubble elements
#define MAX_WIDTH 40

int length;
char *moo;
char buffer[256];

int main(int argc, char** argv)
{   
    if(argc == 1) {
        fgets(buffer, sizeof(buffer), stdin);
        buffer[strlen(buffer)-1]='\0';

        if(strlen(buffer) > 0) {
            moo = buffer;
        }
        else{
            moo = "lol";
        }
    }
    else if(strcmp(argv[1], "-40plus") == 0){
        moo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec est purus, elementum ac pharetra quis, varius vitae nisi. Aenean molestie nisi ut congue ultrices. Sed et lectus..";
    }
    else if(strcmp(argv[1], "-2lines") == 0){
        moo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo enim nec.";
    }
    else if(argc == 2){
        moo = argv[1];
    }

    //moo[strlen(moo)] = *moo;
    printf("Mooing: %s\n",moo);
    length = strlen(moo);
    cow(moo);

    return(0);
}

void cow(char *moo)
{
    int i;
    // if a bubble will just fit on one line, we'll do it all in one place. nice n ez.
    if(length < (MAX_WIDTH + 1)){
        printf(" ");
        for(i = 0; i < (length + 2); i++) printf("_");
        printf("\n< %s >\n ",moo);
        for(i = 0; i < (length + 2); i++) printf("-");
        printf("\n");
    }
    else{
        // we gonna need a bigger bubble
        bubble(moo);
    }
    printf("        \\   ^__^\n");
    printf("         \\  (oo)\\_______\n");
    printf("            (__)\\       )\\/\\\n");
    printf("                ||----w |\n");
    printf("                ||     ||\n");
}

void bubble(char *moo)
{
    int i, j, k;
    int max_lines = ceil((float)length / MAX_WIDTH);
    printf("You asked for %d characters. This will be %d lines.\n",length,max_lines);

    printf(" ");
    for(i = 0; i < (MAX_WIDTH + 2); i++) printf("_");
    printf("\n/ ");

    // 2 lines is also fine
    if(length > (MAX_WIDTH) && length <= (MAX_WIDTH * 2)){
        for(i = 0; i < MAX_WIDTH; i++) {
                printf("%c",moo[i]);
            }

        printf(" \\");

        printf("\n\\ ");
        for(j = i; j < (MAX_WIDTH * 2) && j < length; j++) {
            printf("%c",moo[j]);
        }

        for(k = (j - MAX_WIDTH); k < MAX_WIDTH; k++) printf(" "); 

        printf(" /");
    }

    else{
        // first line
        for(i = 0; i < MAX_WIDTH; i++) {
            printf("%c",moo[i]);
        }

        printf(" \\");


        // last line
        printf("\n\\ ");
        for(i = 0; i < MAX_WIDTH; i++) {
            printf("%c",moo[i]);
        }
        printf(" /");
    }
    printf("\n ");
    for(i = 0; i < (MAX_WIDTH + 2); i++) printf("-");
    printf("\n");
}